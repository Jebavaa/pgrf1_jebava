package objectData;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Pyramid extends Object3D
{

    public Pyramid(List<Integer> pixelValue)
    {
        super(pixelValue);

        // Geometrie
        vb.add(new Point3D(-1, -1, 1.1));
        vb.add(new Point3D(1, -1, 1.1));
        vb.add(new Point3D(1, 1, 1.1));
        vb.add(new Point3D(-1, 1, 1.1));

        vb.add(new Point3D(0, 0, 1.7));


        // Topologie
        ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(0,1,1,2,2,3,3,0,0,4,1,4,2,4,3,4));
        addIndices(list);
    }
}
