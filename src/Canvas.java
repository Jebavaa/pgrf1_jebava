import objectData.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import rasterData.Presentable;
import rasterData.RasterImage;
import rasterData.RasterImageBI;
import rasterOps.*;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Canvas
{


    private final @NotNull JFrame frame;
    private final @NotNull JPanel panel;
    private final @NotNull
    RasterImage<Integer> img;
    private final @NotNull Presentable<Graphics> presenter;
    private final @NotNull TrivialLiner<Integer> liner;
    private final @NotNull WireRenderer<Integer> wireRenderer;
    private final @NotNull List<Object3D> scene;
    Camera camera;
    Object3D activeObject = null;
    double scale = 1;
    int x, y = 0;
    boolean ortogonalni = true;
    boolean firstPerson = false;

    public Canvas(int width, int height)
    {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("Grafika : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final @NotNull RasterImageBI auxRasterImage = new RasterImageBI(width, height);
        img = auxRasterImage;
        presenter = auxRasterImage;
        liner = new TrivialLiner<>();
        scene = new ArrayList<>();
        double cameraSpeed = 0.030;

        Cube cube = new Cube(Arrays.asList(0xFFFF00, 0xDAF7A6));

        // animovaná kostka
        Cube animatedCube = new Cube(Arrays.asList(0x444444));
        Mat4RotXYZ matrix = new Mat4RotXYZ(Math.toRadians(45),Math.toRadians(45),Math.toRadians(0));
        animatedCube.setModel(animatedCube.getModel().mul(matrix));
        animatedCube.setModel(new Mat4Scale(0.5, 0.5, 0.5).mul(animatedCube.getModel()));


        Lines lines = new Lines(Arrays.asList(0x00FF00, 0xFF0000, 0x0000FF));

        Pyramid pyramid = new Pyramid(Arrays.asList(0x655555));

        WaveLine waveLine = new WaveLine(Arrays.asList(0x900C3F));

        Timer timer = new Timer(20, e -> { // animace kostky
                animation(animatedCube);
        });
        timer.start();

        // View
        camera = new Camera(
                new Vec3D(-4, -4, 2), // position
                Math.toRadians(60), // azimuth
                Math.toRadians(-30),  // zenith
                Math.toRadians(90), // radius
                false // first person
        );


        // Projection
        // MVP - Model, View, Projection
        Mat4PerspRH projectionMatrix = new Mat4PerspRH(Math.toRadians(60), height / (double) width, 1, 200); // úhel kamery, poměr, minimal render distance, render distance
        wireRenderer = new WireRenderer(img, liner, camera.getViewMatrix(), projectionMatrix);
        scene.add(cube);
        scene.add(lines);
        scene.add(pyramid);
        scene.add(animatedCube);
        scene.add(waveLine);


        panel = new JPanel()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        panel.addMouseMotionListener(new MouseAdapter()
        {
            @Override
            public void mouseDragged(MouseEvent e)
            {
                clear();

                //region Otáčení myší
                if (e.getY() > y && e.isShiftDown()) // down
                {
                    camera = camera.addZenith(Math.toRadians(-1));
                }
                if (e.getY() < y && e.isShiftDown()) // up
                {
                    camera = camera.addZenith(Math.toRadians(1));
                }
                if (e.getX() > x && e.isShiftDown()) // left
                {
                    camera = camera.addAzimuth(Math.toRadians(1));
                }
                if (e.getX() < x && e.isShiftDown()) // right
                {
                    camera = camera.addAzimuth(Math.toRadians(-1));
                }
                //endregion

                //region Posouvání myší
                if (e.getY() > y)  // down
                {
                    camera = camera.up(cameraSpeed);
                }
                if (e.getY() < y) // up
                {
                    camera = camera.down(cameraSpeed);
                }
                if (e.getX() > x)  // left
                {
                    camera = camera.left(cameraSpeed);
                }
                if (e.getX() < x) // right
                {
                    camera = camera.right(cameraSpeed);
                }
                //endregion

                y = e.getY();
                x = e.getX();


                present();
            }
        });

        //region Oddalování a přibližování kamery
        panel.addMouseWheelListener(new MouseAdapter()
        {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e)
            {
                clear();

                if (e.getWheelRotation() < 0)
                {
                    camera = camera.forward(cameraSpeed*5);
                }
                else
                {
                    camera = camera.backward(cameraSpeed*5);
                }
                wireRenderer.setView(camera.getViewMatrix());
                present();
            }
        });
        //endregion


        panel.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                clear();

                //region  První/třetí osoba pohledu
                if (e.getKeyCode() == KeyEvent.VK_P) // Přepnutí na 3rd person / 1st person
                {
                    if(firstPerson)
                    {
                        camera = camera.withFirstPerson(false);
                        firstPerson = false;
                    }
                    else
                    {
                        camera = camera.withFirstPerson(true);
                        firstPerson = true;
                    }
                }
                //endregion

                //region Ortogonální/perspektivní pohled
                if (e.getKeyCode() == KeyEvent.VK_O){
                    if(ortogonalni){
                        Mat4OrthoRH projectionMatrix = new Mat4OrthoRH(5,5,1,200);
                        wireRenderer.setProjectionMatrix(projectionMatrix);
                        ortogonalni = false;

                    }
                    else{
                        Mat4PerspRH projectionMatrix = new Mat4PerspRH(Math.toRadians(60), height / (double) width, 1, 200);
                        wireRenderer.setProjectionMatrix(projectionMatrix);
                        ortogonalni = true;
                    }
                }
                //endregion

                //region Přepínání ovládání
                if (e.getKeyCode() == KeyEvent.VK_1) // Ovládání kamery
                {
                    activeObject = null;
                    frame.setTitle("Grafika camera mode");
                }
                if (e.getKeyCode() == KeyEvent.VK_2) // Ovládání kostky
                {
                    activeObject = cube;
                    frame.setTitle("Grafika cube mode");
                }
                if (e.getKeyCode() == KeyEvent.VK_3) // Ovládání jehlanu
                {
                    activeObject = pyramid;
                    frame.setTitle("Grafika jehlan mode");
                }
                if (e.getKeyCode() == KeyEvent.VK_4) // Ovládání vlnovky
                {
                    activeObject = waveLine;
                    frame.setTitle("Grafika vlnovka mode");
                }
                if (e.getKeyCode() == KeyEvent.VK_5) // Ovládání animované kostky
                {
                    activeObject = animatedCube;
                    frame.setTitle("Grafika animovaná kostka mode");
                }
                //endregion


                if(activeObject == null)
                {
                    //region Posouvání kamery
                    if (e.getKeyCode() == KeyEvent.VK_D && !e.isShiftDown())
                    {
                        camera = camera.right(cameraSpeed);
                    }
                    if (e.getKeyCode() == KeyEvent.VK_A && !e.isShiftDown())
                    {
                        camera = camera.left(cameraSpeed);
                    }
                    if (e.getKeyCode() == KeyEvent.VK_W && !e.isShiftDown())
                    {
                        camera = camera.up(cameraSpeed);
                    }
                    if (e.getKeyCode() == KeyEvent.VK_S && !e.isShiftDown())
                    {
                        camera = camera.down(cameraSpeed);
                    }
                    //endregion

                    //region Otáčení kamery
                    if (e.getKeyCode() == KeyEvent.VK_A && e.isShiftDown())
                    {
                        camera = camera.addAzimuth(Math.toRadians(-5));
                    }
                    if (e.getKeyCode() == KeyEvent.VK_D && e.isShiftDown())
                    {
                        camera = camera.addAzimuth(Math.toRadians(5));
                    }
                    if (e.getKeyCode() == KeyEvent.VK_W && e.isShiftDown())
                    {
                        camera = camera.addZenith(Math.toRadians(5));
                    }
                    if (e.getKeyCode() == KeyEvent.VK_S && e.isShiftDown())
                    {
                        camera = camera.addZenith(Math.toRadians(-5));
                    }
                    //endregion

                    present();

                    return;
                }




                //region Posunuti objektu
                Mat4Transl mat4Transl = new Mat4Transl(0, 0, 0);

                if (e.getKeyCode() == KeyEvent.VK_W) // up
                {
                     mat4Transl = new Mat4Transl(0, 0, 1);
                }
                if (e.getKeyCode() == KeyEvent.VK_S) // down
                {
                     mat4Transl = new Mat4Transl(0, 0, -1);
                }
                if (e.getKeyCode() == KeyEvent.VK_A && !e.isShiftDown()) // right on y
                {
                     mat4Transl = new Mat4Transl(0, 1, 0);
                }
                if (e.getKeyCode() == KeyEvent.VK_D && !e.isShiftDown()) // right on x
                {
                     mat4Transl = new Mat4Transl(1, 0, 0);
                }
                if (e.getKeyCode() == KeyEvent.VK_A && e.isShiftDown()) // left on y
                {
                     mat4Transl = new Mat4Transl(0, -1, 0);
                }
                if (e.getKeyCode() == KeyEvent.VK_D && e.isShiftDown()) // left on x
                {
                     mat4Transl = new Mat4Transl(-1, 0, 0);
                }
                Mat4 model = activeObject.getModel().mul(mat4Transl);
                activeObject.setModel(model);
                //endregion

                //region Rotace objektu
                Mat4RotXYZ mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(0),Math.toRadians(0),Math.toRadians(0));

                if (e.getKeyCode() == KeyEvent.VK_Q && e.isShiftDown()) // Z +
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(0),Math.toRadians(0),Math.toRadians(10));
                }
                if (e.getKeyCode() == KeyEvent.VK_E && e.isShiftDown()) // Z -
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(0),Math.toRadians(0),Math.toRadians(-10));
                }
                if (e.getKeyCode() == KeyEvent.VK_Q && !e.isShiftDown()) // X +
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(10),Math.toRadians(0),Math.toRadians(0));
                }
                if (e.getKeyCode() == KeyEvent.VK_E && !e.isShiftDown()) // X -
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(-10),Math.toRadians(0),Math.toRadians(0));
                }
                if (e.getKeyCode() == KeyEvent.VK_R && !e.isShiftDown()) // Y +
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(0),Math.toRadians(10),Math.toRadians(0));
                }
                if (e.getKeyCode() == KeyEvent.VK_R && e.isShiftDown()) // Y -
                {
                    mat4RotXYZ = new Mat4RotXYZ(Math.toRadians(0),Math.toRadians(-10),Math.toRadians(0));
                }
                Mat4 model2 = activeObject.getModel().mul(mat4RotXYZ);
                activeObject.setModel(model2);
                //endregion objektu

                //region Scale objektu    chvíli nefunguje, ale pak se vzpamatuje

                if (e.getKeyCode() == KeyEvent.VK_K) // smaller
                {
                    scale = scale - 0.01;
                    activeObject.setModel(new Mat4Scale(scale, scale, scale).mul(activeObject.getModel()));
                }
                if (e.getKeyCode() == KeyEvent.VK_L) // bigger
                {
                    scale = scale + 0.01;
                    activeObject.setModel(new Mat4Scale(scale, scale, scale).mul(activeObject.getModel()));
                }

                //endregion

                present();
            }


        });


        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.grabFocus();
    }
    public void animation(Object3D animatedCube)
    {
        clear();
        animatedCube.setModel((new Mat4RotXYZ(Math.toRadians(-1), Math.toRadians(1), Math.toRadians(1))).mul(animatedCube.getModel()));
        present();
    }


    public void clear()
    {
        img.clear(0x2f2f2f);
    }


    public void present(Graphics graphics)
    {
        presenter.present(graphics);
    }

    public void present()
    {
        wireRenderer.setView(camera.getViewMatrix());
        wireRenderer.renderScene(scene);
        final @Nullable Graphics g = panel.getGraphics();
        if (g != null)
        {
            presenter.present(g);
        }
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> {
            new Canvas(800, 600).present();
        });
    }


}
