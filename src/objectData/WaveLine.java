package objectData;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WaveLine extends Object3D
{
    public WaveLine(List<Integer> pixelValue)
    {

        super(pixelValue);
        int index = 0;

        // Geometrie
        for (double i = 0.0; i < 30.00; i += 0.1)
        {
            vb.add(new Point3D(i, Math.sin(i), 0));
            vb.add(new Point3D(i + 0.05, Math.sin(i + 0.05), 0));

            // Topologie
            ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(index, index + 1));
            addIndices(list);

            index++;
        }
    }
}
