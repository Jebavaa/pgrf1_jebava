package rasterOps;

import objectData.Object3D;
import objectData.Polygon2D;
import org.jetbrains.annotations.NotNull;
import rasterData.RasterImage;
import transforms.*;

import java.util.ArrayList;
import java.util.List;

public class WireRenderer<P>
{

    private Liner<Integer> liner;
    private Mat4 projectionMatrix;
    private Object3D object3D;
    private RasterImage<Integer> img;

    private Mat4 view;

    public WireRenderer(final @NotNull RasterImage<Integer> img,
                        Liner<Integer> liner, Mat4 view, Mat4 projectionMatrix) {
        this.liner = liner;
        this.projectionMatrix = projectionMatrix;
        this.view = view;
        this.img = img;
    }


    public void setView(Mat4 view) {
        this.view = view;
    }

    public void renderScene(List<Object3D> scene){
        for(Object3D object:scene){
            renderObject(object);
        }
    }

    public void renderObject(Object3D object3D){

        int index1,index2;
        Point3D p1,p2;
        int j = 0;

        for(int i = 0; i < object3D.getIb().size(); i+=2){
            index1 = object3D.getIb().get(i);
            index2 = object3D.getIb().get(i + 1);

            p1 = object3D.getVb().get(index1);
            p2 = object3D.getVb().get(index2);

            p1 = p1.mul(object3D.getModel()).mul(view).mul(projectionMatrix);
            p2 = p2.mul(object3D.getModel()).mul(view).mul(projectionMatrix);


            // provede se, jen pokud je v okně viditelnosti
            if(-p1.getW() <= p1.getX() && p1.getX() <= p1.getW() && -p1.getW() <= p1.getY() &&
                    p1.getY() <= p1.getW() && 0 <= p1.getZ() && p1.getZ() <= p1.getW() &&
                    -p2.getW() <= p2.getX() && p2.getX() <= p2.getW() && -p2.getW() <= p2.getY() &&
                    p2.getY() <= p2.getW() && 0 <= p2.getZ() && p2.getZ() <= p2.getW()
            )
            {
                // Dehomogenizace
                Point3D aDehomog = p1.mul(1 / p1.getW());
                Point3D bDehomog = p2.mul(1 / p2.getW());

                Vec3D v1 = transfromToWindow(new Vec3D(aDehomog));
                Vec3D v2 = transfromToWindow(new Vec3D(bDehomog));

                if (j < object3D.pixelValue.size())
                {
                    liner.drawLine(img, (int) Math.round(v1.getX()), (int) Math.round(v1.getY()),
                            (int) Math.round(v2.getX()), (int) Math.round(v2.getY()), object3D.pixelValue.get(j));
                    j++;
                }
                if (j == object3D.pixelValue.size())
                {
                    j = 0;
                }
            }

        }
    }

    private Vec3D transfromToWindow(Vec3D point){
        return point.mul(new Vec3D(1,-1,1))
                .add(new Vec3D(1,1,0))
                .mul(new Vec3D((img.getWidth()-1)/2,(img.getHeight()-1)/2,1));
    }

    public void setProjectionMatrix(Mat4 projectionMatrix)
    {
        this.projectionMatrix = projectionMatrix;
    }
    public Mat4 getProjectionMatrix()
    {
        return this.projectionMatrix;
    }
}
