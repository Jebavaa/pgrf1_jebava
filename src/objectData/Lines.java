package objectData;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Lines extends Object3D
{
    public Lines(List<Integer> pixelValue)
    {

        super(pixelValue);
        // Geometrie
        vb.add(new Point3D(-2, -2, 2));
        vb.add(new Point3D(-2, -2, -2));
        vb.add(new Point3D(2, -2, -2));
        vb.add(new Point3D(-2, 2, -2));

        // Topologie
        ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1, 2, 0, 1, 1, 3));
        addIndices(list);
    }
}
